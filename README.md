# english_learn_admin

#### 介绍
一个在线英语学习平台

#### 账号
老师: 13000000001
学生：15980764243
密码通用： 123456

#### 组件信息
Crumb  // 当前位置
SelectCourseClass  // 选择课程-班级-班级人数导航栏
InsTable  // 表格
leftTreeList // 左侧树形目录
SelectCourseBox // 选择课程弹窗

#### 配置文件注意点
```
module.exports = {
  // publicPath: '/zyzh_web/', // 该配置是用于编译打包部署用的，平时是注释
  devServer: {
    open: process.env.NODE_ENV === 'development',
    host: '0.0.0.0',
    port: 9123,
    https: false,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_BASE_API, // 设置你调用的接口域名和端口号 别忘了加http
        changeOrigin: true,
        pathRewrite: {
          '^/api': '' // 这里理解成用‘/api’代替target里面的地址，后面组件中我们掉接口时直接用api代替 比如我要调用'http://40.00.100.100:3002/user/add'，直接写‘/api/user/add’即可
        }
      }
    }, // string | Object
    watchOptions: {
      poll: true
    }
  }
}
```
