import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/layout'

Vue.use(VueRouter)

const routes = [{
  path: '/login',
  name: 'login',
  component: () => import('@/views/login/Login.vue')
},
// 首页
{
  path: '/',
  component: Layout,
  redirect: '/index',
  children: [
    {
      path: 'index',
      name: 'index',
      component: () => import('@/views/index/Index.vue'),
      meta: {
        title: '首页'
      }
    }
  ]
},
// 学生-课程
{
  path: '/student/course',
  component: Layout,
  redirect: '/student/course/index',
  meta: {
    title: '课程'
  },
  children: [{
    path: 'index',
    name: 'courseIndex',
    component: () => import('@/views/student/course/Course.vue'),
    meta: {
      title: '学生课程'
    }
  }, {
    path: 'study',
    name: 'Study',
    component: () => import('@/views/student/course/study/Study.vue'),
    meta: {
      title: '课程学习'
    }
  }, {
    path: 'task',
    name: 'Task',
    component: () => import('@/views/student/course/task/Task.vue'),
    meta: {
      title: '课程任务'
    }
  }, {
    path: 'taskDetail',
    name: 'TaskDetail',
    component: () => import('@/views/student/course/task/Detail.vue'),
    meta: {
      title: '课程任务详情'
    }
  }, {
    path: 'onlineLesson',
    name: 'onlineLesson',
    component: () => import('@/views/student/course/onlineLesson/OnlineLesson.vue'),
    meta: {
      title: '在线课堂'
    },
    children: [{
      path: 'studentOnlineLessonView',
      name: 'studentOnlineLessonView',
      component: () => import('@/views/student/course/onlineLesson/classView.vue'),
      meta: {
        title: '课堂详情'
      }
    }, {
      path: 'studentOnlineHistory',
      name: 'studentOnlineHistory',
      component: () => import('@/views/student/course/onlineLesson/onlineHistory.vue'),
      meta: {
        title: '历史在线课堂'
      }
    }]
  }, {
    path: 'analyse',
    name: 'Analyse',
    component: () => import('@/views/student/course/analyse/Analyse.vue'),
    meta: {
      title: '课程分析'
    }
  }, {
    path: 'examine',
    name: 'Examine',
    component: () => import('@/views/student/course/examine/Examine.vue'),
    meta: {
      title: '课程分析'
    }
  }]
},
// 学生-实践
{
  path: '/student/practice',
  component: Layout,
  redirect: '/student/practice/index',
  meta: {
    title: '实践'
  },
  children: [{
    path: 'index',
    name: 'practiceIndex',
    component: () => import('@/views/student/practice/Practice.vue'),
    meta: {
      title: '实践'
    }
  },
  {
    path: 'skill',
    name: 'skill',
    component: () => import('@/views/student/practice/skill/SkillIndex.vue'),
    meta: {
      title: '实践技能'
    }
  },
  {
    path: 'skillDetail',
    name: 'skillDetail',
    component: () => import('@/views/student/practice/skill/Skill.vue'),
    meta: {
      title: '实践技能'
    }
  },
  {
    path: 'skilltrain',
    name: 'skilltrain',
    component: () =>
          import('@/views/student/practice/skilltrain/SkillTrain.vue'),
    meta: {
      title: '实践技能练习'
    }
  },
  {
    path: 'skillhistory',
    name: 'skillhistory',
    component: () =>
          import('@/views/student/practice/skillhistory/SkillHistory.vue'),
    meta: {
      title: '实践技能历史情况'
    }
  },
  {
    path: 'skillreport',
    name: 'skillreport',
    component: () =>
          import('@/views/student/practice/skillreport/SkillReport.vue'),
    meta: {
      title: '实践技能报告'
    }
  },
  {
    path: 'hearinghome',
    name: 'hearinghome',
    component: () =>
          import('@/views/student/practice/hearing/hearingHome.vue'),
    meta: {
      title: '听力训练'
    }
  },
  {
    path: 'hearing',
    name: 'hearing',
    component: () =>
          import('@/views/student/practice/hearing/hearing.vue'),
    meta: {
      title: '听力训练'
    }
  },
  {
    path: 'hearingreport',
    name: 'hearingreport',
    component: () =>
          import('@/views/student/practice/hearing/hearingReport.vue'),
    meta: {
      title: '听力训练报告'
    }
  },
  {
    path: 'hearingdetails',
    name: 'hearingdetails',
    component: () =>
          import('@/views/student/practice/hearing/hearingDetails.vue'),
    meta: {
      title: '完成情况页面'
    }
  },
  {
    path: 'hearinghistory',
    name: 'hearinghistory',
    component: () =>
          import('@/views/student/practice/hearing/hearingHistory.vue'),
    meta: {
      title: '听力训练历史情况'
    }
  },
  {
    path: 'skillfollow',
    name: 'skillfollow',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollow.vue'),
    meta: {
      title: '口语评测'
    }
  },
  {
    path: 'skillfollowread',
    name: 'skillfollowread',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowRead.vue'),
    meta: {
      title: '口语测评'
    }
  },
  {
    path: 'skillfollowreport',
    name: 'skillfollowreport',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowReport.vue'),
    meta: {
      title: '跟读报告'
    }
  },
  {
    path: 'skillfollowsuccess',
    name: 'skillfollowsuccess',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowSuccess.vue'),
    meta: {
      title: '完成情况'
    }
  },
  {
    path: 'skillfollowhistory',
    name: 'skillfollowhistory',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowHistory.vue'),
    meta: {
      title: '跟读历史情况'
    }
  },
  {
    path: 'skillfollowdubselect',
    name: 'skillfollowdubselect',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowDubSelect.vue'),
    meta: {
      title: '配音选择角色'
    }
  },
  {
    path: 'skillfollowdublist',
    name: 'skillfollowdublist',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowDubList.vue'),
    meta: {
      title: '配音列表'
    }
  },
  {
    path: 'skillfollowdubranklist',
    name: 'skillfollowdubranklist',
    component: () =>
          import('@/views/student/practice/skillfollowread/SkillFollowDubRankList.vue'),
    meta: {
      title: '配音排行'
    }
  },
  {
    path: 'caseAnalyse',
    name: 'caseAnalyse',
    component: () => import('@/views/student/practice/caseAnalyse/caseAnalyse.vue'),
    meta: {
      title: '案例分析'
    },
    children: [{
      path: 'caseAnalyseView',
      name: 'caseAnalyseView',
      component: () => import('@/views/student/practice/caseAnalyse/caseAnalyseView.vue'),
      meta: {
        title: '案例分析作答'
      }
    }, {
      path: 'caseAnalyseReport',
      name: 'caseAnalyseReport',
      component: () => import('@/views/student/practice/caseAnalyse/caseAnalyseReport.vue'),
      meta: {
        title: '案例分析报告'
      }
    }, {
      path: 'caseAnalyseHistory',
      name: 'caseAnalyseHistory',
      component: () => import('@/views/student/practice/caseAnalyse/caseAnalyseHistory.vue'),
      meta: {
        title: '案例分析历史情况'
      }
    }]
  },
  {
    path: 'interactionHome',
    name: 'interactionHome',
    component: () =>
      import('@/views/student/practice/interaction/interactionHome'),
    meta: {
      title: '互动实训'
    }
  },
  {
    path: 'interactionStart',
    name: 'interactionStart',
    component: () =>
      import('@/views/student/practice/interaction/interactionStart'),
    meta: {
      title: '实训详情'
    }
  },
  {
    path: 'interactionScore',
    name: 'interactionScore',
    component: () =>
      import('@/views/student/practice/interaction/interactionScore'),
    meta: {
      title: '查看评分'
    }
  },
  {
    path: 'interactionScoring',
    name: 'interactionScoring',
    component: () =>
      import('@/views/student/practice/interaction/interactionScoring'),
    meta: {
      title: '组长评分'
    }
  },
  {
    path: 'multiple',
    name: 'multiple',
    component: () =>
      import('@/views/student/practice/multiple'),
    meta: {
      title: '综合案例'
    }
  },
  {
    path: 'multipleMain',
    name: 'multipleMain',
    component: () =>
      import('@/views/student/practice/multiple/multipleMain.vue'),
    meta: {
      title: '综合案例-首页'
    }
  },
  {
    path: 'linkList',
    name: 'linkList',
    component: () =>
      import('@/views/student/practice/multiple/linkList.vue'),
    meta: {
      title: '综合案例-环节列表'
    }
  },
  {
    path: 'taskList',
    name: 'taskList',
    component: () =>
      import('@/views/student/practice/multiple/taskList.vue'),
    meta: {
      title: '综合案例-任务列表'
    }
  },
  {
    path: 'answer',
    name: 'answer',
    component: () =>
      import('@/views/student/practice/multiple/answer.vue'),
    meta: {
      title: '综合案例-答题'
    }
  },
  {
    path: 'caseFinished',
    name: 'caseFinished',
    component: () =>
      import('@/views/student/practice/multiple/caseFinished.vue'),
    meta: {
      title: '综合案例-案例完成情况'
    }
  },
  {
    path: 'taskFinished',
    name: 'taskFinished',
    component: () =>
      import('@/views/student/practice/multiple/taskFinished.vue'),
    meta: {
      title: '综合案例-任务完成情况'
    }
  },
  {
    path: 'caseHistory',
    name: 'caseHistory',
    component: () =>
      import('@/views/student/practice/multiple/caseHistory.vue'),
    meta: {
      title: '综合案例-历史情况'
    }
  }
  ]
},
// 学生-个人中心
{
  path: '/student/center',
  component: Layout,
  redirect: '/student/center/index',
  meta: {
    title: '个人中心'
  },
  children: [{
    path: 'index',
    name: 'centerIndex',
    component: () => import('@/views/student/center/index.vue'),
    meta: {
      title: '个人中心'
    }
  }]
},
// 教师-个人中心
{
  path: '/teacher/center',
  component: Layout,
  redirect: '/teacher/center/index',
  meta: {
    title: '个人中心'
  },
  children: [{
    path: 'index',
    name: 'centerIndex',
    component: () => import('@/views/teacher/center/index.vue'),
    meta: {
      title: '个人中心'
    }
  }]
},
// 教师-管理
{
  path: '/teacher/manage',
  component: Layout,
  redirect: '/teacher/manage/index',
  meta: {
    title: '管理'
  },
  children: [{
    path: 'index',
    name: 'manageIndex',
    component: () => import('@/views/teacher/manage/Manage.vue'),
    meta: {
      title: '管理'
    }
  }, {
    path: 'class-manage',
    name: 'classManage',
    component: () => import('@/views/teacher/manage/class/ClassManage.vue'),
    meta: {
      title: '班级管理'
    }
  }, {
    path: 'course-setting',
    name: 'CourseSetting',
    component: () => import('@/views/teacher/manage/course/CourseSetting.vue'),
    meta: {
      title: '课程设置'
    }
  }, {
    path: 'resourse-manage',
    name: 'ResourseManage',
    component: () => import('@/views/teacher/manage/resource/ResourseManage.vue'),
    meta: {
      title: '资源管理'
    }
  }, {
    path: 'case-manage',
    name: 'caseManage',
    component: () => import('@/views/teacher/manage/case/caseManage.vue'),
    meta: {
      title: '案例管理'
    }
  }, {
    path: 'case-manage-view',
    name: 'caseManageView',
    component: () => import('@/views/teacher/manage/case/caseManageView.vue'),
    meta: {
      title: '案例管理详情'
    }
  }, {
    path: 'interactionManage',
    name: 'interactionManage',
    component: () => import('@/views/teacher/manage/case/interactionManage.vue'),
    meta: {
      title: '互动实训'
    },
    children: [{
      path: 'interactionView',
      name: 'interactionView',
      component: () => import('@/views/teacher/manage/case/interactionView'),
      meta: {
        title: '互动实训详情'
      }
    }, {
      path: 'interactionOpen',
      name: 'interactionOpen',
      component: () => import('@/views/teacher/manage/case/interactionOpen'),
      meta: {
        title: '互动实训选择班级'
      }
    }, {
      path: 'interactionDoing',
      name: 'interactionDoing',
      component: () => import('@/views/teacher/manage/case/interactionDoing'),
      meta: {
        title: '互动实训进行环节'
      }
    }, {
      path: 'interactionGroup',
      name: 'interactionGroup',
      component: () => import('@/views/teacher/manage/case/interactionGroup'),
      meta: {
        title: '互动实训分组'
      }
    }, {
      path: 'interactionAnswer',
      name: 'interactionAnswer',
      component: () => import('@/views/teacher/manage/case/interactionAnswer'),
      meta: {
        title: '互动实训作答'
      }
    }, {
      path: 'interactionComment',
      name: 'interactionComment',
      component: () => import('@/views/teacher/manage/case/interactionComment'),
      meta: {
        title: '互动实训评分'
      }
    }, {
      path: 'interactionCommentOther',
      name: 'interactionCommentOther',
      component: () => import('@/views/teacher/manage/case/interactionCommentOther'),
      meta: {
        title: '互动实训小组评分'
      }
    }, {
      path: 'interactionCommentPersonal',
      name: 'interactionCommentPersonal',
      component: () => import('@/views/teacher/manage/case/interactionCommentPersonal'),
      meta: {
        title: '互动实训个人评分'
      }
    }, {
      path: 'interactionHistory',
      name: 'interactionHistory',
      component: () => import('@/views/teacher/manage/case/interactionHistory'),
      meta: {
        title: '互动实训历史'
      }
    }, {
      path: 'interactionHistoryView',
      name: 'interactionHistoryView',
      component: () => import('@/views/teacher/manage/case/interactionHistoryView'),
      meta: {
        title: '互动实训历史成绩'
      }
    }, {
      path: 'interactionHistoryComment',
      name: 'interactionHistoryComment',
      component: () => import('@/views/teacher/manage/case/interactionHistoryComment'),
      meta: {
        title: '互动实训历史评分'
      }
    }, {
      path: 'interactionHistoryCommentOther',
      name: 'interactionHistoryCommentOther',
      component: () => import('@/views/teacher/manage/case/interactionHistoryCommentOther'),
      meta: {
        title: '互动实训历史小组评分'
      }
    }, {
      path: 'interactionHistoryCommentPersonal',
      name: 'interactionHistoryCommentPersonal',
      component: () => import('@/views/teacher/manage/case/interactionHistoryCommentPersonal'),
      meta: {
        title: '互动实训历史个人评分'
      }
    }]
  }]
},
// 教师-课程
{
  path: '/teacher/course',
  component: Layout,
  redirect: '/teacher/course/index',
  meta: {
    title: '课程'
  },
  children: [{
    path: 'index',
    name: 'teacherCourseIndex',
    component: () => import('@/views/teacher/course/Course.vue'),
    meta: {
      title: '教师课程'
    },
    children: [{
      path: 'onlineLessonView',
      name: 'onlineLessonView',
      component: () => import('@/views/teacher/course/onlineLesson/classView.vue'),
      meta: {
        title: '在线课堂'
      }
    }, {
      path: 'onlineHistory',
      name: 'onlineHistory',
      component: () => import('@/views/teacher/course/onlineLesson/onlineHistory.vue'),
      meta: {
        title: '历史在线课堂'
      }
    }]
  }
  // {
  //   path: 'study-progress',
  //   name: 'StudyProgress',
  //   component: () => import('@/views/teacher/course/studyProgress/StudyProgress.vue'),
  //   meta: {
  //     title: '学习进度'
  //   }
  // }, {
  //   path: 'task-manage',
  //   name: 'TaskManage',
  //   component: () => import('@/views/teacher/course/taskManage/TaskManage.vue'),
  //   meta: {
  //     title: '学习进度'
  //   }
  // }
  ]
}
]

const router = new VueRouter({
  routes
})
// const originalPush = VueRouter.prototype.push;
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err);
// };

export default router
