import Cookies from 'js-cookie'

const TokenKey = 'admin_token'
const ClassId = 'class_id'
const ResourceLessonId = 'resource_lesson_id'

export function getToken () {
  return Cookies.get(TokenKey)
}

export function setToken (token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken () {
  return Cookies.remove(TokenKey)
}

export function getClassId () {
  return Cookies.get(ClassId)
}

export function setClassId (val) {
  return Cookies.set(ClassId, val)
}

export function removeClassId () {
  return Cookies.remove(ClassId)
}

export function getResourceLessonId () {
  return Cookies.get(ResourceLessonId)
}

export function setResourceLessonId (val) {
  return Cookies.set(ResourceLessonId, val)
}

export function removeResourceLessonId () {
  return Cookies.remove(ResourceLessonId)
}
