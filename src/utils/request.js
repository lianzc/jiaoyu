import axios from 'axios'
import { Message } from 'element-ui'
import { getToken, getClassId, getResourceLessonId } from '@/utils/auth'

axios.defaults.baseURL = process.env.NODE_ENV === 'development'
  ? '/api'
  : process.env.VUE_APP_BASE_API
// create an axios instance
const service = axios.create({
  baseURL:
    process.env.NODE_ENV === 'development'
      ? '/api'
      : process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers['token'] = getToken() // token
    config.headers['classId'] = getClassId() // 班级ID
    config.headers['resourceLessonId'] = getResourceLessonId() // 课程ID
    return config
  },
  error => {
    // do something with request error
    window.console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    return response.data
  },
  error => {
    Message({
      message: error.response.data
        ? error.response.data.message
        : error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
