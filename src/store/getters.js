const getters = {
  userInfo: state => state.user.userInfo,
  loginInfo: state => state.user.loginInfo,
  classList: state => state.user.classList
}
export default getters
