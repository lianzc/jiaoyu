import { getUserDetail, getClassResource } from '@/api/overall'

const state = {
  userInfo: sessionStorage.getItem('userInfo') ? JSON.parse(sessionStorage.getItem('userInfo')) : {},
  loginInfo: sessionStorage.getItem('loginInfo') ? JSON.parse(sessionStorage.getItem('loginInfo')) : {},
  classList: sessionStorage.getItem('classList') ? JSON.parse(sessionStorage.getItem('classList')) : {}
}

const mutations = {
  SET_USER_INFO: (state, data) => {
    state.userInfo = data
  },
  SET_LOGIN_INFO: (state, data) => {
    state.loginInfo = data
  },
  SET_CLASS_LIST: (state, data) => {
    state.classList = data
  }
}

const actions = {
  // 获取用户信息
  async setUserInfo ({ commit }) {
    const res = await getUserDetail()
    if (res.code === '200') {
      sessionStorage.setItem('userInfo', JSON.stringify(res.data))
      commit('SET_USER_INFO', res.data)
    }
  },
  // 获取用户信息
  async setLoginInfo ({ commit }, data) {
    sessionStorage.setItem('loginInfo', JSON.stringify(data))
    commit('SET_LOGIN_INFO', data)
  },
  //  获取课程列表
  async setClassList ({ commit }, data) {
    const res = await getClassResource()
    if (res.code === '200') {
      sessionStorage.setItem('classList', JSON.stringify(res.data))
      commit('SET_CLASS_LIST', res.data)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
