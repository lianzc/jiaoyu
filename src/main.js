import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/permission' // permission control
import '@/assets/css/reset.css'
import '@/assets/css/base.css'
import '@/assets/css/yixin.css'
import checkApiServer from './utils/checkApiServer' // 检验request服务
import _ from 'lodash' // 引入工具库

// 引入组件
import Crumb from '@/components/Crumb.vue'
Vue.config.productionTip = false

Vue.prototype._ = _

Vue.component('crumb', Crumb)
Vue.use(ElementUI)
Vue.use(checkApiServer)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
