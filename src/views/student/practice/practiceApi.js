import request from '@/utils/request'
export class Practice {
  static async getSkillList () { // 获取实践列表
    return request({
      url: '/openApi/student/training/skill/index',
      method: 'post'
    })
  }
  static async testStart (data) { // 开始训练
    return request({
      url: '/openApi/student/training/skill/start',
      method: 'post',
      data
    })
  }
  static async updateDuration (data) { // 更新作答时长
    return request({
      url: '/openApi/student/training/skill/updateDuration',
      method: 'post',
      data
    })
  }
  static async submit (data) { // 提交实践
    return request({
      url: '/openApi/student/training/skill/submit',
      method: 'post',
      data
    })
  }
  static async getReport (data) { // 获取实践报告
    return request({
      url: '/openApi/student/training/skill/report',
      method: 'post',
      data
    })
  }
  static async finishedList (data) { // 获取实践技能完成情况
    return request({
      url: '/openApi/student/training/skill/finishedList',
      method: 'post',
      data
    })
  }
  static async islike (data) { // 点赞
    return request({
      url: '/openApi/student/training/skill/like',
      method: 'post',
      data
    })
  }
  static async answerAnalyse (data) { // 答案解析
    return request({
      url: '/openApi/student/training/skill/answerAnalyse',
      method: 'post',
      data
    })
  }
  // 收藏题目
  static async collectTest (data) {
    return request({
      url: 'openApi/student/training/skill/collectSubject',
      method: 'post',
      data
    })
  }
  // 取消收藏
  static async cancelCollectTest (data) {
    return request({
      url: '/openApi/student/training/skill/cancelCollectSubject',
      method: 'post',
      data
    })
  }
  // 获得历史
  static async getHistory (data) {
    return request({
      url: '/openApi/student/training/skill/history',
      method: 'post',
      data
    })
  }
}
