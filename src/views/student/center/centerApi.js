import request from '@/utils/request'
export class Center {
  static async getUserDetail () {
    return request({
      url: '/openApi/overall/getUserDetail',
      method: 'post'
    })
  }
  static async updateUser (data) {
    return request({
      url: '/openApi/login/updateUser',
      method: 'post',
      data
    })
  }
  // 发送二维码
  static async sendSms (data) {
    return request({
      url: '/openApi/login/sendSms',
      method: 'post',
      data
    })
  }
  // 修改密码
  static async updatePwd (data) {
    return request({
      url: '/openApi/login/updatePwd',
      method: 'post',
      data
    })
  }
  // 验证验证码
  static async validateCode (data) {
    return request({
      url: '/openApi/login/validateSms',
      method: 'post',
      data
    })
  }
  // 获取收藏
  static async getStuSubjectCollect (data) {
    return request({
      url: '/openApi/overall/getStuSubjectCollect',
      method: 'post',
      data
    })
  }
  // 取消收藏
  static async cancelCollectTest (data) {
    return request({
      url: '/openApi/student/training/skill/cancelCollectSubject',
      method: 'post',
      data
    })
  }
  // 常见问题
  static async getSystemStuQuestion (data) {
    return request({
      url: '/openApi/overall/getSystemStuQuestion',
      method: 'post',
      data
    })
  }
  // 关于我们
  static async getSystemAbout (data) {
    return request({
      url: '/openApi/login/getSystemAbout',
      method: 'post',
      data
    })
  }
}
