import request from '@/utils/request'

// 获取数据
export function getGlobal (param) {
  return request({
    url: `/lesson-setting/global?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}`,
    method: 'get'
  })
}
// 切换设置，key 包含 lesson_is_opened, training_is_opened, top_quality_is_opened, answer_analyse_is_opened,task_send_is_opened,operation_language
export function globalSwitch (param) {
  return request({
    url: `/lesson-setting/global/switch/${param.key}?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&value=${param.value}`,
    method: 'post'
  })
}
