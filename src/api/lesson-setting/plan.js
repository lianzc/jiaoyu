import request from '@/utils/request'

// 获取数据
export function getPlan (param) {
  return request({
    url: `/lesson-setting/plan?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}`,
    method: 'get'
  })
}

// 为班级制定学习计划 开关
export function planSwitch (param) {
  return request({
    url: `/lesson-setting/plan/switch?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&isOpen=${param.isOpen}`,
    method: 'post'
  })
}

// 学习结束后设置
export function studyFinishedContinueType (param) {
  return request({
    url: `/lesson-setting/plan/training_set/study_finished_continue_type?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&type=${param.type}`,
    method: 'post'
  })
}

// 学习时间设置
export function setTime (param) {
  return request({
    url: `/lesson-setting/plan/${param.url}?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&${param.keys[0]}=${param.value[0]}&${param.keys[1]}=${param.value[1]}`,
    method: 'post'
  })
}
