import request from '@/utils/request'

// 获取数据
export function getGlobal () {
  return request({
    url: '/lesson-setting/global',
    method: 'get'
  })
}
