import request from '@/utils/request'

// 获取数据
export function getTraining (param) {
  return request({
    url: `/lesson-setting/training?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}`,
    method: 'get'
  })
}

// 课程学习设置开关
export function setTraining (url, param) {
  return request({
    url: `/lesson-setting/training/${url}?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&${param.key}=${param.value}`,
    method: 'post'
  })
}
