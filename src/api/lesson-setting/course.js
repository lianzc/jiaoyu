import request from '@/utils/request'

// 获取数据
export function getCourse (param) {
  return request({
    url: `/lesson-setting/course?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}`,
    method: 'get'
  })
}

// 课程学习设置开关
// /lesson-setting/course/study?school_id=624273132001165312&lesson_is_opened=0&resource_lesson_id=626383332573184000&class_id=657218784214958080&tea_id=654346104019615744
export function courseStudy (url, param) {
  return request({
    url: `/lesson-setting/course/${url}?class_id=${param.class_id}&resource_lesson_id=${param.resource_lesson_id}&school_id=${param.school_id}&tea_id=${param.tea_id}&${param.key}=${param.value}`,
    method: 'post'
  })
}
