import request from '@/utils/request'
// 课程分析
export function getAnalyse (data) {
  return request({
    url: '/openApi/student/lesson/analyse/getStudentStudyAnalysis',
    method: 'post',
    data
  })
}
