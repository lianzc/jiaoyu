import request from '@/utils/request'
import { checkApiServer } from '@/utils/checkApiServer'

const controller = '/openApi/student/training/multiple/'

export async function multipleIndex (data) {
  let res = await request({
    url: `${controller}index`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function caseDetail (data) {
  let res = await request({
    url: `${controller}caseDetail`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getTaskInfo (data) {
  let res = await request({
    url: `${controller}getTaskInfo`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getSubjectInfo (data) {
  let res = await request({
    url: `${controller}getSubjectInfo`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function submitSubject (data) {
  let res = await request({
    url: `${controller}submitSubject`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function pushAnswerTime (data) {
  let res = await request({
    url: `${controller}pushAnswerTime`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getCaseAnalysisDetail (data) {
  let res = await request({
    url: `${controller}getCaseAnalysisDetail`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getTaskDetail (data) {
  let res = await request({
    url: `${controller}getTaskDetail`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getTrainingDetail (data) {
  let res = await request({
    url: `${controller}getTrainingDetail`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getCaseFinishedDetail (data) {
  let res = await request({
    url: `${controller}getCaseFinishedDetail`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function like (data) {
  let res = await request({
    url: `${controller}like`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getCaseHistoryInfo (data) {
  let res = await request({
    url: `${controller}getCaseHistoryInfo`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function answerAnalyse (data) {
  let res = await request({
    url: `${controller}answerAnalyse`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}

export async function getOralSubjectInfo (data) {
  let res = await request({
    url: `${controller}getOralSubjectInfo`,
    method: 'post',
    data
  })
  return checkApiServer(res)
}
