import request from '@/utils/request'

const controller = '/openApi/student/training/speech/'

export function getClassResource (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 口语开始训练
export function speechStart (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 口语评测目录
export function speechIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}
// 口语训练
export function speechMain (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}
// 更新作答时间
export function speechUpdateDuration (data) {
  return request({
    url: `${controller}updateDuration`,
    method: 'post',
    data
  })
}
// 口语录音提交
export function speechSubmit (data) {
  return request({
    url: `${controller}submit`,
    method: 'post',
    data
  })
}
// 口语报告
export function speechReport (data) {
  return request({
    url: `${controller}report`,
    method: 'post',
    data
  })
}
// 口语评测完成
export function speechFinishedList (data) {
  return request({
    url: `${controller}finishedList`,
    method: 'post',
    data
  })
}
// 口语点赞
export function speechLike (data) {
  return request({
    url: `${controller}like`,
    method: 'post',
    data
  })
}
// 口语历史
export function speechHistory (data) {
  return request({
    url: `${controller}history`,
    method: 'post',
    data
  })
}
// 口语配音选择角色
export function speechEnterDubRole (data) {
  return request({
    url: `${controller}enterDubRole`,
    method: 'post',
    data
  })
}
// 口语配音列表
export function speechDub (data) {
  return request({
    url: `${controller}dub`,
    method: 'post',
    data
  })
}
// 口语配音提交
export function speechDubsubmit (data) {
  return request({
    url: `${controller}dubsubmit`,
    method: 'post',
    data
  })
}
// 口语配音排行
export function speechDubRankList (data) {
  return request({
    url: `${controller}dubRankList`,
    method: 'post',
    data
  })
}
// 配音排行点赞
export function speechLikeDub (data) {
  return request({
    url: `${controller}likeDub`,
    method: 'post',
    data
  })
}
