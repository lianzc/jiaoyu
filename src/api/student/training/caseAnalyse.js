import request from '@/utils/request'

const controller = '/openApi/student/training/caseAnalyse/'

// 案例分析目录
export function caseIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 互动案例分析目录
export function interactionindex (data) {
  return request({
    url: `${controller}interactionindex`,
    method: 'post',
    data
  })
}

// 案例分析主页
export function caseAnalyseMain (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 案例开始作答
export function caseAnalyseStart (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 更新作答时长
export function updateDuration (data) {
  return request({
    url: `${controller}updateDuration`,
    method: 'post',
    data
  })
}

// 案例提交作答
export function caseAnalyseSubmit (data) {
  return request({
    url: `${controller}submit`,
    method: 'post',
    data
  })
}

// 案例分析报告
export function caseAnalyseReport (data) {
  return request({
    url: `${controller}report`,
    method: 'post',
    data
  })
}

// 案例分析完成情况
export function finishedList (data) {
  return request({
    url: `${controller}finishedList`,
    method: 'post',
    data
  })
}

// 案例点赞
export function like (data) {
  return request({
    url: `${controller}like`,
    method: 'post',
    data
  })
}

// 优秀案例分析列表
export function excellentList (data) {
  return request({
    url: `${controller}excellentList`,
    method: 'post',
    data
  })
}

// 案例分析历史情况
export function history (data) {
  return request({
    url: `${controller}history`,
    method: 'post',
    data
  })
}
