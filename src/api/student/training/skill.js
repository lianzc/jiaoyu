import request from '@/utils/request'

const controller = '/openApi/student/training/skill/'

// 实践技能目录
export function skillIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 开始训练
export function skillStart (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}
