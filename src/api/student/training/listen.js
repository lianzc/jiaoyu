import request from '@/utils/request'

const controller = '/openApi/student/training/listen/'

// 开始训练
export function index (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 开始训练
export function start (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 开始训练
export function main (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 提交训练
export function submit (data) {
  return request({
    url: `${controller}submit`,
    method: 'post',
    data
  })
}

// 听力训练报告
export function report (data) {
  return request({
    url: `${controller}report`,
    method: 'post',
    data
  })
}

// 答题解析
export function answerAnalyse (data) {
  return request({
    url: `${controller}answerAnalyse`,
    method: 'post',
    data
  })
}

// 收藏题目
export function collectSubject (data) {
  return request({
    url: `${controller}collectSubject`,
    method: 'post',
    data
  })
}

// 取消收藏
export function cancelCollectSubject (data) {
  return request({
    url: `${controller}cancelCollectSubject`,
    method: 'post',
    data
  })
}

// 听力任务完成情况
export function finishedList (data) {
  return request({
    url: `${controller}finishedList`,
    method: 'post',
    data
  })
}

// 点赞
export function like (data) {
  return request({
    url: `${controller}like`,
    method: 'post',
    data
  })
}

// 更新作答时长
export function updateDuration (data) {
  return request({
    url: `${controller}updateDuration`,
    method: 'post',
    data
  })
}

// 听力训练历史情况
export function history (data) {
  return request({
    url: `${controller}history`,
    method: 'post',
    data
  })
}
