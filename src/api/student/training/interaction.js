import request from '@/utils/request'

const controller = '/openApi/student/training/interaction/'

// 目录
export function index (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 开始训练
export function start (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 主页
export function main (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 进入小组评分
export function groupMark (data) {
  return request({
    url: `${controller}groupMark`,
    method: 'post',
    data
  })
}

// 小组评分提交
export function commitScore (data) {
  return request({
    url: `${controller}commitScore`,
    method: 'post',
    data
  })
}

// 进入个人评分
export function personScore (data) {
  return request({
    url: `${controller}personScore`,
    method: 'post',
    data
  })
}

// 个人评分组员列表
export function personScoreGroupIndex (data) {
  return request({
    url: `${controller}personScoreGroupIndex`,
    method: 'post',
    data
  })
}

// 更新提交作答
export function updateAnswer (data) {
  return request({
    url: `${controller}updateAnswer`,
    method: 'post',
    data
  })
}

// 环节被评分接口
export function nodeByMark (data) {
  return request({
    url: `${controller}nodeByMark`,
    method: 'post',
    data
  })
}

// 环节被评分明细接口
export function nodeByMarkDetail (data) {
  return request({
    url: `${controller}nodeByMarkDetail`,
    method: 'post',
    data
  })
}

// 个人评分提交接口
export function commitPersonScore (data) {
  return request({
    url: `${controller}commitPersonScore`,
    method: 'post',
    data
  })
}

// 个人得分查看
export function personScoreLook (data) {
  return request({
    url: `${controller}personScoreLook`,
    method: 'post',
    data
  })
}

// 个人得分查看详情
export function personScoreLookDetail (data) {
  return request({
    url: `${controller}personScoreLookDetail`,
    method: 'post',
    data
  })
}

// 互动实训报告
export function report (data) {
  return request({
    url: `${controller}report`,
    method: 'post',
    data
  })
}

// 历史记录环节查看
export function historyNode (data) {
  return request({
    url: `${controller}historyNode`,
    method: 'post',
    data
  })
}

// 环节评分列表
export function nodeMark (data) {
  return request({
    url: `${controller}nodeMark`,
    method: 'post',
    data
  })
}

// 环节评分明细
export function nodeMarkDetail (data) {
  return request({
    url: `${controller}nodeMarkDetail`,
    method: 'post',
    data
  })
}

// 个人评分查看
export function personMarkLook (data) {
  return request({
    url: `${controller}personMarkLook`,
    method: 'post',
    data
  })
}

// 个人评分查看详情
export function personMarkLookDetail (data) {
  return request({
    url: `${controller}personMarkLookDetail`,
    method: 'post',
    data
  })
}
