import request from '@/utils/request'

const controller = '/openApi/student/index/'

// 首页内容
export function main (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 推荐课程
export function recommendLesson (data) {
  return request({
    url: `${controller}recommendLesson`,
    method: 'post',
    data
  })
}

// 加入班级
export function addClassByCode (data) {
  return request({
    url: `${controller}addClassByCode`,
    method: 'post',
    data
  })
}

// 课程介绍
export function lessonInfo (data) {
  return request({
    url: `${controller}lessonInfo`,
    method: 'post',
    data
  })
}
