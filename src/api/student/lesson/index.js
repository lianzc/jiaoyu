import request from '@/utils/request'

const controller = '/openApi/student/lesson/index/'

// 课程首页
export function main (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 获取课程id
export function getLessonId (data) {
  return request({
    url: `${controller}getLessonId`,
    method: 'post',
    data
  })
}
