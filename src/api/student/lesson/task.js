import request from '@/utils/request'

const controller = '/openApi/student/lesson/task/'

// 任务首页
export function taskIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}
// 任务的状态验证
export function checkTaskFlag (data) {
  return request({
    url: `${controller}checkTaskFlag`,
    method: 'post',
    data
  })
}
// 任务训练主页
export function taskMain (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}
// 任务完成情况
export function taskFinishedList (data) {
  return request({
    url: `${controller}finishedList`,
    method: 'post',
    data
  })
}

// 开始任务
export function taskStart (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 更新任务作答时长
export function updateDuration (data) {
  return request({
    url: `${controller}updateDuration`,
    method: 'post',
    data
  })
}

// 提交任务
export function taskSubmit (data) {
  return request({
    url: `${controller}submit`,
    method: 'post',
    data
  })
}
