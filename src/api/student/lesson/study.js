import request from '@/utils/request'

const controller = '/openApi/student/lesson/study/'

// 课程目录
export function studyIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 课程学习
export function studyMain (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 课程评论
export function commentList (data) {
  return request({
    url: `${controller}commentList`,
    method: 'post',
    data
  })
}

// 课程笔记
export function noteList (data) {
  return request({
    url: `${controller}noteList`,
    method: 'post',
    data
  })
}

// 更新学习进度时长
export function updateDuration (data) {
  return request({
    url: `${controller}updateDuration`,
    method: 'post',
    data
  })
}

// 更新学习进度百分比
export function updateProcessRate (data) {
  return request({
    url: `${controller}updateProcessRate`,
    method: 'post',
    data
  })
}

// 发表评论
export function addComment (data) {
  return request({
    url: `${controller}addComment`,
    method: 'post',
    data
  })
}

// 发表跟评
export function addCommentReply (data) {
  return request({
    url: `${controller}addCommentReply`,
    method: 'post',
    data
  })
}

// 发表笔记
export function addNote (data) {
  return request({
    url: `${controller}addNote`,
    method: 'post',
    data
  })
}

// 公开笔记
export function publicNote (data) {
  return request({
    url: `${controller}publicNote`,
    method: 'post',
    data
  })
}

// 课程测验首页
export function testMain (data) {
  return request({
    url: `${controller}testMain`,
    method: 'post',
    data
  })
}

// 进入课程测验
export function enterTest (data) {
  return request({
    url: `${controller}enterTest`,
    method: 'post',
    data
  })
}

// 更新测验进度时长
export function updateTestDuration (data) {
  return request({
    url: `${controller}updateTestDuration`,
    method: 'post',
    data
  })
}

// 验证是否能进入学习或者测验
export function checkLessonSetDetail (data) {
  return request({
    url: `${controller}checkLessonSetDetail`,
    method: 'post',
    data
  })
}

// 我的历史测验
export function historyTest (data) {
  return request({
    url: `${controller}historyTest`,
    method: 'post',
    data
  })
}

// 课程学习完成情况表
export function finishedList (data) {
  return request({
    url: `${controller}finishedList`,
    method: 'post',
    data
  })
}

// 提交测验
export function submitTest (data) {
  return request({
    url: `${controller}submitTest`,
    method: 'post',
    data
  })
}

// 提交测验
export function testReport (data) {
  return request({
    url: `${controller}testReport`,
    method: 'post',
    data
  })
}

// 题目解析
export function answerAnalyse (data) {
  return request({
    url: `${controller}answerAnalyse`,
    method: 'post',
    data
  })
}

// 收藏题目
export function collectTest (data) {
  return request({
    url: `${controller}collectTest`,
    method: 'post',
    data
  })
}

// 取消收藏
export function cancelCollectTest (data) {
  return request({
    url: `${controller}cancelCollectTest`,
    method: 'post',
    data
  })
}

// 课程统计
export function lessonStat (data) {
  return request({
    url: `${controller}lessonStat`,
    method: 'post',
    data
  })
}
