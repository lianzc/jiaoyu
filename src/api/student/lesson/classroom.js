import request from '@/utils/request'

const controller = '/openApi/student/lesson/classroom/'

// 首页内容
export function classCalendar (data) {
  return request({
    url: `${controller}classCalendar`,
    method: 'post',
    data
  })
}

// 点击进入课堂
export function entryOnlineCourse (data) {
  return request({
    url: `${controller}entryOnlineCourse`,
    method: 'post',
    data
  })
}

// 获取播放资源
export function current (data) {
  return request({
    url: `${controller}current`,
    method: 'post',
    data
  })
}

// 获取交流列表
export function exchangeList (data) {
  return request({
    url: `${controller}exchangeList`,
    method: 'post',
    data
  })
}

// 发送消息
export function addExchange (data) {
  return request({
    url: `${controller}addExchange`,
    method: 'post',
    data
  })
}

// 获取笔记
export function getOnlineLessonStuNote (data) {
  return request({
    url: `${controller}getOnlineLessonStuNote`,
    method: 'post',
    data
  })
}

// 保存笔记
export function saveOnlineLessonStuNote (data) {
  return request({
    url: `${controller}saveOnlineLessonStuNote`,
    method: 'post',
    data
  })
}

// 获取活动列表
export function activityList (data) {
  return request({
    url: `${controller}activityList`,
    method: 'post',
    data
  })
}

// 获取分享展示详情
export function shareScoreDetail (data) {
  return request({
    url: `${controller}shareScoreDetail`,
    method: 'post',
    data
  })
}

// 活动-分享-评分
export function shareGrade (data) {
  return request({
    url: `${controller}shareGrade`,
    method: 'post',
    data
  })
}

// 获取点名签到详情
export function signInMain (data) {
  return request({
    url: `${controller}signInMain`,
    method: 'post',
    data
  })
}

// 签到/进行签到
export function signIn (data) {
  return request({
    url: `${controller}signIn`,
    method: 'post',
    data
  })
}

// 即时测验
export function testMain (data) {
  return request({
    url: `${controller}testMain`,
    method: 'post',
    data
  })
}

// 更新即时测验进度时长
export function updateTestDuration (data) {
  return request({
    url: `${controller}updateTestDuration`,
    method: 'post',
    data
  })
}

// 提交测验
export function testSubmit (data) {
  return request({
    url: `${controller}testSubmit`,
    method: 'post',
    data
  })
}

// 获取历史在线课堂
export function getHistoryOnlineLessonList (data) {
  return request({
    url: `${controller}getHistoryOnlineLessonList`,
    method: 'post',
    data
  })
}
