import request from '@/utils/request'

// 获取用户详细信息
export function getUserDetail (data) {
  return request({
    url: '/openApi/overall/getUserDetail',
    method: 'post',
    data
  })
}

// 全局--班级课程选择
export function getClassResource (data) {
  return request({
    url: '/openApi/overall/getClassResource',
    method: 'post',
    data
  })
}

// 全局--查找题目信息
export function findSubject (data) {
  return request({
    url: '/openApi/overall/findSubject',
    method: 'post',
    data
  })
}
