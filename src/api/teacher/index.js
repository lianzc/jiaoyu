import request from '@/utils/request'

const controller = '/openApi/teacher/index/'

// 首页内容
export function main (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 推荐课程
export function recommendLesson (data) {
  return request({
    url: `${controller}recommendLesson`,
    method: 'post',
    data
  })
}

// 机器翻译
export function machineTrans (data) {
  return request({
    url: '/openApi/overall/machineTrans',
    method: 'post',
    data
  })
}

// 智能批改
export function brainCorrect (data) {
  return request({
    url: '/openApi/overall/brainCorrect',
    method: 'post',
    data
  })
}
