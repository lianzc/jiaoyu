import request from '@/utils/request'

const controller = '/openApi/teacher/task/'

// 任务管理首页
export function taskIndex (data) {
  return request({
    url: `${controller}taskIndex`,
    method: 'post',
    data
  })
}

// 保存、发布任务
export function saveTask (data) {
  return request({
    url: `${controller}saveTask`,
    method: 'post',
    data
  })
}

// 撤回任务
export function reCallTask (data) {
  return request({
    url: `${controller}reCallTask`,
    method: 'post',
    data
  })
}

// 编辑任务
export function editTask (data) {
  return request({
    url: `${controller}editTask`,
    method: 'post',
    data
  })
}

// 任务进度
export function taskRate (data) {
  return request({
    url: `${controller}taskRate`,
    method: 'post',
    data
  })
}
