import request from '@/utils/request'

const controller = '/openApi/teacher/onlineCourse/'

// 按照日历获取课程列表
export function teachingCalendar (data) {
  return request({
    url: `${controller}teachingCalendar`,
    method: 'post',
    data
  })
}

// 添加课程
export function saveOnlineCourse (data) {
  return request({
    url: `${controller}saveOnlineCourse`,
    method: 'post',
    data
  })
}

// 编辑课程
export function updateOnlineCourse (data) {
  return request({
    url: `${controller}updateOnlineCourse`,
    method: 'post',
    data
  })
}

// 获取播放资源
export function getOnlineCoursePlayback (data) {
  return request({
    url: `${controller}getOnlineCoursePlayback`,
    method: 'post',
    data
  })
}

// 获取交流的列表
export function getInterchange (data) {
  return request({
    url: `${controller}getInterchange`,
    method: 'post',
    data
  })
}

// 发送交流数据
export function saveInterchange (data) {
  return request({
    url: `${controller}saveInterchange`,
    method: 'post',
    data
  })
}

// 获取资源
export function getOnlineResource (data) {
  return request({
    url: `${controller}getOnlineResource`,
    method: 'post',
    data
  })
}

// 获取笔记
export function getOnlineLessonStuNote (data) {
  return request({
    url: `${controller}getOnlineLessonStuNote`,
    method: 'post',
    data
  })
}

// 保存笔记
export function saveOnlineLessonStuNote (data) {
  return request({
    url: `${controller}saveOnlineLessonStuNote`,
    method: 'post',
    data
  })
}

// 获取历史在线课堂列表
export function getHistoryOnlineLessonList (data) {
  return request({
    url: `${controller}getHistoryOnlineLessonList`,
    method: 'post',
    data
  })
}

// 获取历史在线课堂获取统计数据
export function getHistoryOnlineLessonStatInfo (data) {
  return request({
    url: `${controller}getHistoryOnlineLessonStatInfo`,
    method: 'post',
    data
  })
}

// 获取在线课堂学生列表
export function getOnlineStuList (data) {
  return request({
    url: `${controller}getOnlineStuList`,
    method: 'post',
    data
  })
}

// 获取在线课堂学生列表 在线状态
export function onlineLessonStuList (data) {
  return request({
    url: `${controller}onlineLessonStuList`,
    method: 'post',
    data
  })
}

// 获取活动列表
export function getOnlineActivity (data) {
  return request({
    url: `${controller}getOnlineActivity`,
    method: 'post',
    data
  })
}

// 上课签到
export function studySign (data) {
  return request({
    url: `${controller}studySign`,
    method: 'post',
    data
  })
}

// 开始点名
export function startRollCall (data) {
  return request({
    url: `${controller}startRollCall`,
    method: 'post',
    data
  })
}

// 保存即时测验
export function saveOnlineTest (data) {
  return request({
    url: `${controller}saveOnlineTest`,
    method: 'post',
    data
  })
}

// 获取测验详情
export function getOnlineTest (data) {
  return request({
    url: `${controller}getOnlineTest`,
    method: 'post',
    data
  })
}

// 删除活动
export function delActivity (data) {
  return request({
    url: `${controller}delActivity`,
    method: 'post',
    data
  })
}

// 获取资源列表
export function getShareResourceList (data) {
  return request({
    url: `${controller}getShareResourceList`,
    method: 'post',
    data
  })
}

// 保存分享
export function saveLessonShare (data) {
  return request({
    url: `${controller}saveLessonShare`,
    method: 'post',
    data
  })
}

// 获取分享展示详情
export function getOnlineLessonShareDetail (data) {
  return request({
    url: `${controller}getOnlineLessonShareDetail`,
    method: 'post',
    data
  })
}
