import request from '@/utils/request'
const controller = '/openApi/teacher/training/trainingInteractionManage/'

// 案例管理首页（互动实训页）
export function interactionManageIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 查询班级集合
export function classList (data) {
  return request({
    url: `${controller}classList`,
    method: 'post',
    data
  })
}

// 互动实训（历史记录和开启案例首页）
export function trainingInteraction (data) {
  return request({
    url: `${controller}trainingInteraction`,
    method: 'post',
    data
  })
}

// 开启案例
export function trainingInteractionStart (data) {
  return request({
    url: `${controller}start`,
    method: 'post',
    data
  })
}

// 互动实训详情内容
export function trainingInteractionMain (data) {
  return request({
    url: `${controller}main`,
    method: 'post',
    data
  })
}

// 互动实训分组（已选班级下的学生和案例分组信息）
export function groupAllocation (data) {
  return request({
    url: `${controller}groupAllocation`,
    method: 'post',
    data
  })
}

// 学生分组提交（分组完成自动开启第一个环节）
export function groupCommit (data) {
  return request({
    url: `${controller}groupCommit`,
    method: 'post',
    data
  })
}

// 学生分组查看（老师已分组）
export function groupShow (data) {
  return request({
    url: `${controller}groupShow`,
    method: 'post',
    data
  })
}

// 学生组长更换
export function changerGroup (data) {
  return request({
    url: `${controller}changerGroup`,
    method: 'post',
    data
  })
}

// 自动分组
export function autoGroup (data) {
  return request({
    url: `${controller}autoGroup`,
    method: 'post',
    data
  })
}

// 环节作答查看（环节进行中查看学生作答）
export function nodeAnswer (data) {
  return request({
    url: `${controller}nodeAnswer`,
    method: 'post',
    data
  })
}

// 查看小组作答（已作答学生小组查看作答）
export function showAnswer (data) {
  return request({
    url: `${controller}showAnswer`,
    method: 'post',
    data
  })
}

// 环节评分查看（环节完成查看学生评分）
export function nodeScore (data) {
  return request({
    url: `${controller}nodeScore`,
    method: 'post',
    data
  })
}

// 查看小组评分
export function showScore (data) {
  return request({
    url: `${controller}showScore`,
    method: 'post',
    data
  })
}

// 小组评分详情
export function groupScoreDetail (data) {
  return request({
    url: `${controller}groupScoreDetail`,
    method: 'post',
    data
  })
}

// 关闭环节（自动开启下一环节，没有则开启个人评分环节）
export function endNode (data) {
  return request({
    url: `${controller}endNode`,
    method: 'post',
    data
  })
}

// 个人评分列表
export function personScoreList (data) {
  return request({
    url: `${controller}personScoreList`,
    method: 'post',
    data
  })
}

// 个人评分查看
export function personScore (data) {
  return request({
    url: `${controller}personScore`,
    method: 'post',
    data
  })
}

// 个人评分明细
export function personScoreDetail (data) {
  return request({
    url: `${controller}personScoreDetail`,
    method: 'post',
    data
  })
}

// 实训结束
export function caseEnd (data) {
  return request({
    url: `${controller}caseEnd`,
    method: 'post',
    data
  })
}

// 实训历史
export function history (data) {
  return request({
    url: `${controller}history`,
    method: 'post',
    data
  })
}
