import request from '@/utils/request'

// 管理首页-课程资源
export function lessonResource (data) {
  return request({
    url: '/openApi/teacher/manage/index/lessonResource',
    method: 'post',
    data
  })
}
