import request from '@/utils/request'

// 我的资源
export function myResource (data) {
  return request({
    url: '/openApi/teacher/manage/resource/my',
    method: 'post',
    data
  })
}

// 共享资源
export function shareResource (data) {
  return request({
    url: '/openApi/teacher/manage/resource/share',
    method: 'post',
    data
  })
}

// 删除资源
export function deleteResource (data) {
  return request({
    url: '/openApi/teacher/manage/resource/delete',
    method: 'post',
    data
  })
}

// 资源库
export function reportResource (data) {
  return request({
    url: '/openApi/teacher/manage/resource/report',
    method: 'post',
    data
  })
}
