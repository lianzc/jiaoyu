import request from '@/utils/request'
const controller = '/openApi/teacher/training/casemanage/'

// 案例管理首页
export function manageIndex (data) {
  return request({
    url: `${controller}index`,
    method: 'post',
    data
  })
}

// 案例分析页面
export function caseAnalyseMain (data) {
  return request({
    url: `${controller}caseAnalyseMain`,
    method: 'post',
    data
  })
}

// 案例分析评分页面
export function caseAnalyseScoreMain (data) {
  return request({
    url: `${controller}caseAnalyseScoreMain`,
    method: 'post',
    data
  })
}

// 提交评分
export function submitCaseAnalyseScore (data) {
  return request({
    url: `${controller}submitCaseAnalyseScore`,
    method: 'post',
    data
  })
}
