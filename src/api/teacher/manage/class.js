import request from '@/utils/request'

// 创建班级
export function saveClass (data) {
  return request({
    url: '/openApi/teacher/manage/class/save',
    method: 'post',
    data
  })
}

// 创建班级
export function classList (data) {
  return request({
    url: '/openApi/teacher/manage/class/list',
    method: 'post',
    data
  })
}

// 查看班级
export function viewClass (data) {
  return request({
    url: '/openApi/teacher/manage/class/view',
    method: 'post',
    data
  })
}

// 查找学生
export function findStudent (data) {
  return request({
    url: '/openApi/teacher/manage/class/findStudent',
    method: 'post',
    data
  })
}

// 添加学生
export function addStudent (data) {
  return request({
    url: '/openApi/teacher/manage/class/addStudent',
    method: 'post',
    data
  })
}
// 修改班级
export function updateClass (data) {
  return request({
    url: '/openApi/teacher/manage/class/update',
    method: 'post',
    data
  })
}

// 删除学生
export function deleteStudent (data) {
  return request({
    url: '/openApi/teacher/manage/class/deleteStudent',
    method: 'post',
    data
  })
}
