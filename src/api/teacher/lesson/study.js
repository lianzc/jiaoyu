import request from '@/utils/request'

const controller = '/openApi/teacher/lesson/study/'

// 课程学习进度
export function studyList (data) {
  return request({
    url: `${controller}studyList`,
    method: 'post',
    data
  })
}
