import request from '@/utils/request'

const controller = '/openApi/teacher/lesson/analyse/'

// 学习分析-异步调度任务
export function studyAnalysisAsync (data) {
  return request({
    url: `${controller}studyAnalysisAsync`,
    method: 'post',
    data
  })
}

// 学习分析-查询老师下的所有班级
export function getClass () {
  return request({
    url: '/openApi/overall/getClassList',
    method: 'post'
  })
}
// 学习分析-查询老师下的所有学生
export function getStudy (data) {
  return request({
    url: '/openApi/teacher/manage/class/view',
    method: 'post',
    data
  })
}
// 学习分析-查询
export function getAnalysisAsync (data) {
  return request({
    url: '/openApi/teacher/lesson/studyAnalysis/getStudentStudyAnalysis',
    method: 'post',
    data
  })
}
