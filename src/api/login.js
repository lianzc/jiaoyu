import request from '@/utils/request'

const controller = '/openApi/login/'
// 用户登录
export function userLogin (data) {
  return request({
    url: `${controller}userLogin`,
    method: 'post',
    data
  })
}

// 用户验证
export function userValidate (data) {
  return request({
    url: `${controller}userValidate`,
    method: 'post',
    data
  })
}

// 发送验证码
export function sendSms (data) {
  return request({
    url: `${controller}sendSms`,
    method: 'post',
    data
  })
}

// 验证验证码
export function validateSms (data) {
  return request({
    url: `${controller}validateSms`,
    method: 'post',
    data
  })
}

// 用户注册
export function userAdd (data) {
  return request({
    url: `${controller}userAdd`,
    method: 'post',
    data
  })
}

// 修改用户密码
export function updatePwd (data) {
  return request({
    url: `${controller}updatePwd`,
    method: 'post',
    data
  })
}

// 忘记密码--》修改用户密码
export function userUpdatePwd (data) {
  return request({
    url: `${controller}userUpdatePwd`,
    method: 'post',
    data
  })
}

// 更新用户信息
export function updateUser (data) {
  return request({
    url: `${controller}updateUser`,
    method: 'post',
    data
  })
}

// 获取系统信息--协议
export function getSystemAgreement (data) {
  return request({
    url: `${controller}getSystemAgreement`,
    method: 'post',
    data
  })
}

// 获取系统信息--关于我们
export function getSystemAbout (data) {
  return request({
    url: `${controller}getSystemAbout`,
    method: 'post',
    data
  })
}
