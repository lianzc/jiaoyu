import request from '@/utils/request'

// 获取schoolId
export function getSchoolId (params) {
  return request({
    url: '/biz-data/school-id',
    method: 'get',
    params
  })
}
